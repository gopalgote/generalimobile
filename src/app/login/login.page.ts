import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';

//import { DatePicker } from '@ionic-native/date-picker';

//import { DatePicker } from '@ionic-native/date-picker';

import {DatePicker} from '@ionic-native/date-picker/ngx';
import {DatePipe} from '@angular/common'; 



@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  showRegister: boolean = false;
  showLogin: boolean = true;
  signin = "SIGN IN";
  signup = "SIGN UP";
  contentHide:boolean=false;
  toogle:boolean=true;
  loginForm:boolean=false;
  hideLandingtxt:boolean=true
  myDate:string;
  birthDate:any;

  constructor(
    private route:Router,
    //private datePicker: DatePicker
     private datePicker:DatePicker,
     private datePipe:DatePipe
  ) { 
  }

  ngOnInit() {
  }

  gotoRegister() {
    this.showRegister = true;
    this.showLogin = false;
    this.signin="SIGN UP";
    this.signup="LOGIN";
    // var fname=document.getElementById('fname')
    // fname.style.height="50pt"
  }
  gotoLogin() {
    this.showRegister = false;
    this.showLogin = true;
    this.signin="SIGN IN";
    this.signup="SIGN UP";
    this.route.navigate(['home']);
  }

  toggle(){
    if(this.showRegister!=true){
  var move = document.getElementById('move');
  var generaliLogo=document.getElementById('generaliLogo')
  var couple=document.getElementById('couple');

    if(move.style.height===""){
      move.style.height="264px";
      generaliLogo.style.marginTop="0px";
      couple.style.display="none"
    }
    else{
        if (move.style.height === "550px") {
            move.style.height = "264px";
            generaliLogo.style.marginTop="0px";
            couple.style.display="none"

        } else {
            move.style.height = "550px";
            generaliLogo.style.marginTop="35px"
            couple.style.display="block"
        }
      }
      this.loginForm=!this.loginForm;
      this.hideLandingtxt=!this.hideLandingtxt;
    }
   }

   loginShow(){
     this.toggle();
     this.loginForm=true;
     this.hideLandingtxt=false;
   }
   showLoginForm(){
    this.showRegister = false;
    this.showLogin = true;
   }

  showDatepicker(){
   this.datePicker.show({
  date: new Date(),
  mode: 'date',
  androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
}).then(
  date => {
    
    console.log('Got date: ', date);
    this.birthDate=this.datePipe.transform(date,"dd-MM-yyyy");
},
  err => console.log('Error occurred while getting date: ', err)
);
}  













   // This is the important part!

 collapseSection(element) {
  // get the height of the element's inner content, regardless of its actual size
  var sectionHeight = element.scrollHeight;
  
  // temporarily disable all css transitions
  var elementTransition = element.style.transition;
  element.style.transition = '';
  
  // on the next frame (as soon as the previous style change has taken effect),
  // explicitly set the element's height to its current pixel height, so we 
  // aren't transitioning out of 'auto'
  requestAnimationFrame(function() {
    element.style.height = sectionHeight + 'px';
    element.style.transition = elementTransition;
    
    // on the next frame (as soon as the previous style change has taken effect),
    // have the element transition to height: 0
    requestAnimationFrame(function() {
      element.style.height = 0 + 'px';
    });
  });
  
  // mark the section as "currently collapsed"
  element.setAttribute('data-collapsed', 'true');
}

 expandSection(element) {
  // get the height of the element's inner content, regardless of its actual size
  var sectionHeight = element.scrollHeight;
  
  // have the element transition to the height of its inner content
  element.style.height = sectionHeight + 'px';

  // when the next css transition finishes (which should be the one we just triggered)
  element.addEventListener('transitionend', function(e) {
    // remove this event listener so it only gets triggered once
    element.removeEventListener('transitionend', arguments.callee);
    
    // remove "height" from the element's inline styles, so it can return to its initial value
    element.style.height = null;
  });
  
  // mark the section as "currently not collapsed"
  element.setAttribute('data-collapsed', 'false');
}

//document.querySelector('#toggle-button').addEventListener('click', function(e) {
  collapse(){
  var section = document.querySelector('.section.collapsible');
  var isCollapsed = section.getAttribute('data-collapsed') === 'true';
    
  if(isCollapsed) {
    this.expandSection(section)
    section.setAttribute('data-collapsed', 'false')
  } else {
    this.collapseSection(section)
  }
}
//});

}
