import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  insuranceServices=[
    {
      serviceName:'Find a Doctor',
      svg: '../../../assets/svg/home/doctor.svg'
    },
    {
      serviceName:'eHealth Card',
      svg:'../../../assets/svg/home/patient.svg'
    },
    {
      serviceName:'Make a Claim',
      svg:'../../../assets/svg/home/makeclaim.svg'
    },
    {
      serviceName:'Claim History',
      svg:'../../../assets/svg/home/claimHistory.svg'
    },
    {
      serviceName:'Submit a Pre Auth',
      svg:'../../../assets/svg/home/submitPre.svg'
    },
    {
      serviceName:'Pre Auth Status',
      svg:'../../../assets/svg/home/preAuth.svg'
    },
    {
      serviceName:'Policy Document',
      svg:'../../../assets/svg/home/policy.svg'
    }    
  ];
  healthPArtnerServices=[
    {
      serviceName:'My Digital Doctor',
      svg:'../../../assets/svg/home/digitalDoctor.svg'
    },
    {
      serviceName:'Bria Wellness',
      svg:'../../../assets/svg/home/briaWellness.svg'
    },
    {
      serviceName:'Symtom Checker',
      svg:'../../../assets/svg/home/symtomChecker.svg'
    },
    {
      serviceName:'Text Dummy',
      svg:'../../../assets/svg/home/textDummy.svg'
    }
  ];

  constructor() { }
}
