import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { HomePage } from "./home.page";

import { NgCircleProgressModule } from "ng-circle-progress";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCircleProgressModule.forRoot({
      radius: 30,
      space: -10,
      outerStrokeGradient: true,
      outerStrokeWidth: 3,
      innerStrokeWidth: 2,
      title: "65%",
      subtitle: "Today",
      animateTitle: false,
      animationDuration: 2000,
      showUnits: false,
      showBackground: false,
      clockwise: true,
      startFromZero: false,
      subtitleColor: "gray",
      titleFontSize: "12",
      titleColor: "#c63e5a"
    }),
    RouterModule.forChild([
      {
        path: "",
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
