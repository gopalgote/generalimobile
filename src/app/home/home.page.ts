import { Component } from '@angular/core';
// import  {} from "../services/homepage/service"
import {ServiceService} from '../services/homepage/service.service'
import { NgCircleProgressModule } from 'ng-circle-progress';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  // const seeMore = document.getElementById("seeMoreBtn");
  // const article = document.getElementById("article");

  insuranceServices;
  healthPartner;
  constructor(
    private insuranceService:ServiceService
  ) {
      this.insuranceServices=this.insuranceService.insuranceServices;
      this.healthPartner=this.insuranceService.healthPArtnerServices;
  }
  slideOpts = {
    slidesPerView: 10,
    freeMode: true,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }




  // seeMore.addEventListener("click", () => {
  //     article.classList.toggle("expanded");

  //     const expanded = article.classList.contains("expanded");
  //     if (expanded) {
  //         seeMore.innerHTML = "See Less";
  //     } else {
  //         seeMore.innerHTML = "See More";
  //     }
  // });

}
